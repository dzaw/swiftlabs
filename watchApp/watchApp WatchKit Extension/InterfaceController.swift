//
//  InterfaceController.swift
//  watchApp WatchKit Extension
//
//  Created by dagmara on 17/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet var citeLabel: WKInterfaceLabel!
    @IBAction func refreshClicked() {
        getNewContent()
    }
    
    func getNewContent() {
        //citeLabel.setText("pobieranie...")
        let url = try! String(contentsOf: URL(string: "http://swift3.itcourse.pl/7//f.php" )!)
        citeLabel.setText(url)
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        //getNewContent()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
