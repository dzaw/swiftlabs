//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

/*

var int1 = 1
var double : Double = 1
//var sum = int1 + double
//var test : Double = int1
var test2 : Double = Double(int1)


let ilosc = 5
var napis = "Mam " + String(ilosc) + " apples "
var napis2 = "Mam \(ilosc) jablek"

*/

/*
var x = 1
var y = 2
print(x,y)

var tmp = x
x = y
y = tmp
print(x,y)

(x,y) = (y,x)

print(x,y)


*/


/*
var napis: String?
print(napis)
napis = "abc"
print(napis)
print(napis ?? "default value if nil")
print(napis!)
print(napis as Any)


if let napis2 = napis {
    print(napis2)
}

napis?.append("aaa")

*/

/*

var tablica = [1,2,3,4,5]
tablica.append(contentsOf: [7,8,9])

tablica.count

for el in tablica {
    print(el)
    tablica.append(1)
    //print(tablica)
}

tablica.count
print(tablica)

*/

/*
//zwykle:
func greetings(name:String){
    print("hello \(name)")
}
greetings(name: "swift")

// z podręcznika:
func greetings2(to name:String, at day:String){
    print("hello \(name) at \(day)")
}
greetings2(to: "Adam", at: "friday")

//z opuszczeniem nazw parametrów w wywolaniu:
func greetings3(_ name:String, _ day:String){
    print("hello \(name) at \(day)")
}
greetings3("Dave", "monday")


*/

/*
//wartość losowa
func game(max:Int) -> Int{
    return Int( arc4random() ) % max
}
game(max: 10)


func graj(razy:Int, max:Int, w gra: ((Int)->Int)){
    for i in 1 ... razy {
        gra(max)
    }
}

graj(razy:5, max:10, w: game )

graj(razy:5, max:10, w: {parametr in
    return Int(arc4random()) % parametr
} )


graj(razy:5, max:10) {parametr in
    return Int(arc4random()) % parametr
}


graj(razy:5, max:10) { Int(arc4random()) % $0 }

*/


class Human {
    private var imie:String?
    private weak var znam: Human?
    init () { //init bez imienia
    }
    
    init(imie:String){ //init z imieniem
        self.imie = imie
    }
    
    func getName() -> String{
        print(self.imie ?? "Nie mam imienia")
        return self.imie ?? "brak imienia"
    }
    
    func setName(imie:String) -> String{
        self.imie = imie
        return self.imie!
    }
    
    func meet(person h:Human){
        self.znam = h
    }
    
    deinit {
        print("znikam")
    }
    
}

var john = Human(imie: "John")
//print(john.imie)
// john.imie = "new" - var imie jest private więc nie można zmienić po inicjalizacji
john.getName()

var alex = Human()
alex.getName()
alex.setName(imie: "Alexander")
alex.getName()

var janek:Human? = Human(imie: "Janek")
var olek:Human? = Human(imie: "Olek")

janek?.meet(person: olek!)


