//
//  Response.swift
//  WifiWawa
//
//  Created by dagmara on 15/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import Foundation

struct Response: Codable {
    let result: Result?
}

struct Result: Codable {
    let resource_id:String
    let records:[Record]?
}

struct Record: Codable {
    let x_wgs84:String
    let _id:Int
    let y_wgs84:String
    let nazwa:String
}

/*
struct Response: Codable {
    let main: Main?
    let weather: [Weather]?
    
}

struct Main: Codable {
    let temp:Double?
}

struct Weather:Codable {
    let main:String?
    let description:String?
}
*/
