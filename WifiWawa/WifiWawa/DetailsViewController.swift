//
//  DetailsViewController.swift
//  WifiWawa
//
//  Created by dagmara on 15/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit
import MapKit

class MyPin: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, coordinate:CLLocationCoordinate2D){
        self.title = title
        self.coordinate = coordinate
        
        super.init()
        
    }
}

class DetailsViewController: UIViewController {
    
    var cl :CLLocationManager?
    
    var miasto:String? {
        didSet{
            self.updateInfo()
        }
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true) {print("back btn pressed")}        
    }
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    @IBOutlet weak var cityX: UILabel!
    @IBOutlet weak var cityY: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    let initialLocation = CLLocation(latitude: 51, longitude: 17)
    
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 10000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //var pinArray = [ [],[],[] ]
    
    func updateInfo(){
        if let label = self.cityNameLabel {
            label.text = miasto
    
            if let miasto = self.miasto {
                label.text = miasto
                
                let escapedString = miasto.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                print(escapedString!)
                
                let urlstring = "https://api.um.warszawa.pl/api/action/datastore_search/?resource_id=53ef6c4b-8025-4008-a268-916a66de4cfc"
                //print("URL string \(urlstring)" )
                
                if let url = URL(string: urlstring) {
                    do {
                        let content = try String(contentsOf: url )
                        //print("CONTENT: \(content)" )
                        let resp = try JSONDecoder().decode(Response.self, from: content.data(using: .utf8)! )
                        //print("RESP: \(resp)" )
                        //print("ID: \(resp.result?.records?.first?._id)" )
                        //print("Nazwa: \(resp.result?.records?.first?.nazwa)" )
                        
                        if let X = resp.result?.records?.first?.x_wgs84, let Y = resp.result?.records?.first?.y_wgs84 {
                            cityX.text = "\(String(format: "%.2f", Float(X)!))"
                            cityY.text = "\(String(format: "%.2f", Float(Y)!))"
                            
                            for record in (resp.result?.records)! {
                                let name = record.nazwa
                                let X = record.x_wgs84
                                let Y = record.y_wgs84
                                let pincoordinate = CLLocation(latitude: Double(Y)!, longitude: Double(X)!)
                                let mypin = MyPin(title: name, coordinate: pincoordinate.coordinate)
                                mapView.addAnnotation(mypin)
                                
                            }
                            
                            let pincoordinate = CLLocation(latitude: 52.23, longitude: 21.01)
                            centerMapOnLocation(location: pincoordinate)
                            
                        }
                    }
                    catch let e as NSError {
                        print(e)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centerMapOnLocation(location: initialLocation)
        updateInfo()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
