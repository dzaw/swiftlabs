//
//  ViewController.swift
//  healthApp
//
//  Created by dagmara on 17/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit
import HealthKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let typ = HKQuantityType.quantityType(forIdentifier: .heartRate)
        
        let hk = HKHealthStore()
        hk.requestAuthorization(toShare: [], read: [typ!], completion:{approved,_ in
            if approved {
                var predicate : NSPredicate = HKQuery.predicateForSamples(withStart: Date().addingTimeInterval(-60.0*60), end: Date(), options: HKQueryOptions.strictStartDate)
                
                let q = HKSampleQuery(sampleType: typ!, predicate: predicate, limit: 100, sortDescriptors: nil, resultsHandler: { (_, samples, _) in
                    print(samples)
                })
                
                hk.execute(q)
                
            }
            
            
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

