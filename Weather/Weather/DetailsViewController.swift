//
//  DetailsViewController.swift
//  weather
//
//  Created by dagmara on 15/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit
import MapKit

class DetailsViewController: UIViewController {
    
    var miasto:String? {
        didSet{
            self.updateInfo()
        }
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true) {print("back btn pressed")}        
    }
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var cityTempC: UILabel!
    @IBOutlet weak var cityTempF: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    func updateInfo(){
        if let label = self.cityNameLabel {
            label.text = miasto
    
            if let miasto = self.miasto {
                label.text = miasto
                
                let escapedString = miasto.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                print(escapedString!)
                
                let urlstring = "http://api.openweathermap.org/data/2.5/weather?q=\(escapedString!),pl&APPID=7de811c104439156057e27a2a2fc36b8&units=imperial"
                print("URL string: \(urlstring)" )
                
                if let url = URL(string: urlstring) {
                    do {
                        let content = try String(contentsOf: url )
                        print("CONTENT: \(content)" )
                        let resp = try JSONDecoder().decode(Response.self, from: content.data(using: .utf8)! )
                        
                        print(resp.weather)
                        
                        if let tempF = resp.main?.temp {
                            self.cityTempF.text = "\(tempF)"
                            cityTempC.text = "\( (tempF-32)*5/9 )"
                            
                        }
                        print(resp.weather?.first?.main?.lowercased())
                        
                        if let weatherTypeResp = resp.weather?.first?.main?.lowercased(){
                            print(weatherTypeResp)
                            
                            weatherType.text = weatherTypeResp
                            
                            if let weatherImage = UIImage(named: weatherTypeResp){
                                self.weatherImage.image = weatherImage
                            }
                        }
                        
                    }
                    catch {
                        
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateInfo()
        
        self.cl = CLLocationManager()
        cl?.requestAlwaysAuthorization()
        
        mapView.userTrackingMode = .follow
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
