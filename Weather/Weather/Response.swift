//
//  Response.swift
//  weather
//
//  Created by dagmara on 15/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import Foundation

struct Response: Codable {
    let main: Main?
    let weather: [Weather]?
    
}

struct Main: Codable {
    let temp:Double?
}

struct Weather:Codable {
    let main:String?
    let description:String?
}
