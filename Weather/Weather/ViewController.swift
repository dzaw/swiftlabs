//
//  ViewController.swift
//  weather
//
//  Created by dagmara on 15/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        print("\(segue.identifier!) opened")

        let destinationVC = segue.destination as? DetailsViewController
        
        let button = sender as? UIButton

        destinationVC?.miasto = button?.titleLabel?.text
    }

}

