//
//  GameViewController.swift
//  DotHunt
//
//  Created by Student on 08.04.2017.
//  Copyright © 2017 Student. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    var scene : GameScene? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
         scene = GameScene.newGameScene()

        // Present the scene
        let skView = self.view as! SKView
        skView.presentScene(scene!)
        
        skView.ignoresSiblingOrder = true
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        
        let gr_lewo = UISwipeGestureRecognizer(target: self, action: #selector(GameViewController.skrecWLewo(_:)))
        gr_lewo.direction = .left
        skView.addGestureRecognizer(gr_lewo)
        
        let gr_prawo = UISwipeGestureRecognizer(target: self, action: #selector(GameViewController.skrecWPrawo(_:)))
        gr_prawo.direction = .right
        skView.addGestureRecognizer(gr_prawo)
        
    }
    
    func skrecWLewo(_ gr:UISwipeGestureRecognizer){
        scene?.turnLeft()
    }
    
    func skrecWPrawo(_ gr:UISwipeGestureRecognizer){
        scene?.turnRight()
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
