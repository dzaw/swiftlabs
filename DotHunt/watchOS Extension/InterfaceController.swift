//
//  InterfaceController.swift
//  watchOS Extension
//
//  Created by Student on 08.04.2017.
//  Copyright © 2017 Student. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController, WKCrownDelegate {

    @IBOutlet var skInterface: WKInterfaceSKScene!
    var scene : GameScene?
    
    override func awake(withContext context: Any?) { //odpala się raz kiedy interfejs jest załadowany
        super.awake(withContext: context)
        
        scene = GameScene.newGameScene()
        self.skInterface.presentScene(scene!)
        //let cs = crownSequencer <- tak można tylko tutaj, wewnątrz klasy InterfaceController, więc lepiej (z dowolnego miejsca):
        let cs = WKExtension.shared().rootInterfaceController?.crownSequencer
        cs?.delegate = self
        cs?.focus()
        
    }
    
    var rotationalData = 0.0
    func crownDidRotate(_ crownSequencer: WKCrownSequencer?, rotationalDelta: Double) {
        print(rotationalDelta)
        self.rotationalData = rotationalDelta
    }
    
    func crownDidBecomeIdle(_ crownSequencer: WKCrownSequencer?) {
        print("rotate ended")
        if rotationalData > 0 {
            print("w prawo")
            scene?.turnRight()
        }
        else {
            print("w lewo")
            scene?.turnLeft()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
