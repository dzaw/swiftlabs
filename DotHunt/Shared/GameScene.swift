//
//  GameScene.swift
//  DotHunt
//
//  Created by Student on 08.04.2017.
//  Copyright © 2017 Student. All rights reserved.
//

import SpriteKit
#if os(watchOS)
    import WatchKit
    // <rdar://problem/26756207> SKColor typealias does not seem to be exposed on watchOS SpriteKit
    typealias SKColor = UIColor
#endif

class GameScene: SKScene {
    
    
    fileprivate var label : SKLabelNode?
    fileprivate var spinnyNode : SKShapeNode?

    
    class func newGameScene() -> GameScene {
         guard let scene = SKScene(fileNamed: "GameScene") as? GameScene else {
            print("Failed to load GameScene.sks")
            abort()
        }
        scene.scaleMode = .aspectFill
        
        return scene
    }
    
    var direction = CGPoint(x:1,y:0)
    var obiekt : SKSpriteNode? = nil
    
    
    func setUpScene() {
        if(obiekt == nil){
        obiekt = SKSpriteNode(color: UIColor.red, size: CGSize(width: 20, height: 20))
        self.addChild(obiekt!)
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(GameScene.upd(_:)), userInfo: nil, repeats: true)
        }
    }
    
    func upd(_ timer: Timer){
        obiekt?.position = CGPoint(x: (obiekt?.position.x)! + direction.x*25, y: (obiekt?.position.y)! + direction.y*25)
    }
    
    #if os(watchOS)
    override func sceneDidLoad() {
        self.setUpScene()
    }
    #else
    override func didMove(to view: SKView) {
        self.setUpScene()
    }
    #endif
    
    
    
    func turnRight(){
        print(direction)
        print("w prawo")
        if (direction.x==0 && direction.y==1){
            direction = CGPoint(x: 1, y: 0)
        }else
        if (direction.x == 1 && direction.y == 0){
            direction = CGPoint(x: 0, y: -1)
        }else
        
        if (direction.x == 0 && direction.y == -1){
            direction = CGPoint(x: -1, y: 0)
        }else
        
        if (direction.x == -1 && direction.y == 0){
            direction = CGPoint(x: 0, y: 1)
        }    }
    
    func turnLeft(){
        print(direction)
        print("w lewo")
        if (direction.x==0 && direction.y==1){
            direction = CGPoint(x: -1, y: 0)
        }else
        if (direction.x == -1 && direction.y == 0){
            direction = CGPoint(x: 0, y: -1)
        }else
        
        if (direction.x == 0 && direction.y == -1){
            direction = CGPoint(x: 1, y: 0)
        }else 
        
        if (direction.x == 1 && direction.y == 0){
            direction = CGPoint(x: 0, y: 1)
        }
    }
    
    
    
    
    
    
}
