//
//  Kalkulator.swift
//  Kalkulator
//
//  Created by Blazej Zyglarski on 16.06.2018.
//  Copyright © 2018 Blazej Zyglarski. All rights reserved.
//

import UIKit

class Kalkulator {
    
    func compute(a:Int,b:Int,o:String)->Int{
        switch o{
            case "+": return sum(a: a, b: b)
            case "-": return dif(a: a, b: b)
            case "*": return mult(a: a, b: b)
            case "!": return sil(a: b)
            case "^": return pow(a: a, p: b)
            default: return b
        }
    }
    func sum(a:Int,b:Int)->Int{
        return a + b;
    }
    func dif(a:Int,b:Int)->Int{
        return a - b;
    }
    func mult(a:Int,b:Int)->Int{
        var res = 0
        for _ in 1 ... b {
            res = sum(a: res, b: a)
        }
        return res
    }
    func pow(a:Int,p:Int)->Int{
        var res = 1
        for _ in 1 ... p {
            res = mult(a: res, b: a)
        }
        return res
    }
    func sil(a:Int)->Int{
        if a == 1 {
            return a
        }else{
            return mult(a: sil(a: a-1), b: a)  
        }
    }
    
}
