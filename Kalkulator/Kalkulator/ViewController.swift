//
//  ViewController.swift
//  Kalkulator
//
//  Created by Blazej Zyglarski on 16.06.2018.
//  Copyright © 2018 Blazej Zyglarski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var disp: UILabel!
    
    var actual_value = 0 {
        didSet{
            disp.text = "\(actual_value)"
        }
    }
    
    var oper = ""
    @IBAction func press_number(_ sender: UIButton) {
        
        
        if let nl = sender.titleLabel?.text{
            if let t = disp.text{
                disp.text = "\(t)\(nl)"
            }
            
        }
    }
    
    @IBAction func compute(_ sender: Any) {
        var k = Kalkulator()
        if let nl = disp.text{
            if let val = Int(nl){
                actual_value = k.compute(a:actual_value,b:val,o:oper)
            }
        }
    }
    
    @IBAction func pressC(_ sender: Any) {
        actual_value = 0
    }
    
    
    @IBAction func pressOperator(_ sender: Any) {
        if let nl = disp.text{
            if let val = Int(nl){
                actual_value = val
            }
        }
        disp.text="0"
        let btt = sender as? UIButton
        if let op = btt?.titleLabel?.text{
            oper = op
            
        }
    }
    


}

