//
//  KalkulatorTests.swift
//  KalkulatorTests
//
//  Created by Blazej Zyglarski on 16.06.2018.
//  Copyright © 2018 Blazej Zyglarski. All rights reserved.
//

import XCTest
@testable import Kalkulator

class KalkulatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDiff() {
        
        var k = Kalkulator()
        XCTAssertEqual(k.dif(a: 5, b: 2), 3)
        
    }
    
    func testPerformanceExample() {
        self.measure {
            var k = Kalkulator()
            k.compute(a: 1, b: 2, o: "-")
            k.compute(a: 1, b: 2, o: "+")
            k.compute(a: 6, b: 2, o: "*")
        }
    }
    
    func testSum() {
        var k = Kalkulator()
        XCTAssertEqual(k.sum(a: 2, b: 3), 5)
    }
    
    
    func testMulti() {
        var k = Kalkulator()
        XCTAssertEqual(k.mult(a: 2, b: 2), 4)
    }
    
    
    func testPow() {
        var k = Kalkulator()
        XCTAssertEqual(k.pow(a: 2, p: 3), 8)
    }
    
}
