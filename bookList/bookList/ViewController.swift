//
//  ViewController.swift
//  bookList
//
//  Created by dagmara on 16/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit

struct Book {
    let author:String
    let title:String
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var books = [Book(author: "author 1", title: "book one"),
                 Book(author: "author 2", title: "book two"),
                 Book(author: "author 3", title: "book three"),
                 Book(author: "author 4", title: "book four"),
                 Book(author: "author 5", title: "book five"),
                 Book(author: "author 1", title: "book one"),
                 Book(author: "author 2", title: "book two"),
                 Book(author: "author 3", title: "book three"),
                 Book(author: "author 4", title: "book four"),
                 Book(author: "author 5", title: "book five"),
                 Book(author: "author 1", title: "book one"),
                 Book(author: "author 2", title: "book two"),
                 Book(author: "author 3", title: "book three"),
                 Book(author: "author 4", title: "book four"),
                 Book(author: "author 5", title: "book five")
                 ]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 20
        return books.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "bookCell")
        cell?.textLabel?.text = "\(indexPath.row) : \(books[indexPath.row].title)"
        cell?.detailTextLabel?.text = "\(books[indexPath.row].author)"
        
        //do switcha:
        if let subviews = cell?.contentView.subviews{
            for sw in subviews{
                if let uiswitch = sw as? UISwitch {
                    uiswitch.setOn(false, animated: false)
                }
            }
        }
        
        return cell!
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

