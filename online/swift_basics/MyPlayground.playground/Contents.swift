//: Playground - noun: a place where people can play

import UIKit


// SWIFT BASICS
 
//var str = "Hello, playground"

print("hello world");

// variable
var var_name = "var";

//constant
let let_name = "let";

var i = 1

let j = 2

i += 1
//j += 1


// ------------------------ //

var points = 0;
points += 1;
print(points);
points = points + 1;
print(points);

// ------------------------ //

var desc = "string and number " + String(18)
var desc2 = "string \(points) "

// init with type declaraction
let string:String
let pi:Double

let fl : Float = 30 //converted to float

// ------------------------ //

var a = 1234
var b = 12
var str1 = "radius is " + String(b)
var str2 = "pole of our circle is \(a*b)"

// ------------------------ //

var text = "TeXt teXT"
text.append(contentsOf: "123")
text.lowercased()
text.uppercased()
text.capitalized

// ------------------------ //

var largeNumberWith_  = 1_000_000_000

text.count

// ------------------------ //

//optionals don't need initialization

var s:String?

s == nil

//for optionals
s?.uppercased()

// ------------------------ //

let xx:Double = 12
let yy:Double = 7
let z = xx/yy

// += -+ *= /=

// ------------------------ //

var aa = 5
var bb = 10

(aa,bb)=(bb,aa)

print(aa)
print(bb)

// ------------------------ //
/*
var ars: Int = 7

switch ars {
    case ...(-1):
        print("less than 20")
    case 0...9:
        print("less than 10")
    case 10...20:
        print("less than 20")
    case 20...:
        print("less than 20")
    default:
        print("default")
}
*/

// ------------------------ //

var largest: String
largest = a > b ? "a is largest" : "b is largest"


// ------------------------ //


func fn(val1:String, val2:Int){
    print("string is \(val1)")
    print("and int is \(val2)")
}

fn(val1:"testing", val2:33)

func fn2(_ val1:String, _ val2:Int){
    print("string is \(val1)")
    print("and int is \(val2)")
}

fn2("no param names", 44)

// ------------------------ //

func has10chars(text:String) -> Bool{ // -> returning boolean
    return text.count == 10
}
print(has10chars(text: "blablabla"))
print(has10chars(text: "aaaaaaaaaa"))

func has10chars2(text:String) -> (Bool, Int){ // -> returning boolean
    return (text.count == 10, text.count)
}
has10chars2(text: "blablabla")

// ------------------------ //
//recurent
func factorial(n:Int) -> Int {
    if ( n == 1 ){
        return 1
    }
    else {
        return Int( n * factorial(n: n-1) )
    }
}

//factorial(n: 1)
//factorial(n: 2)
//factorial(n: 3)



// ------------------------ //

//tables
var table = [1,2,3,4]
table.count
table.append(5)
print(table)
table.remove(at: 0)
print(table)

var tabl = [0,0,0,0,0,0]
tabl[0] = Int( (arc4random() % 49) + 1 )   //value in range 0 - 49
tabl[1] = Int( (arc4random() % 49) + 1 )
tabl[2] = Int( (arc4random() % 49) + 1 )
tabl[3] = Int( (arc4random() % 49) + 1 )
tabl[4] = Int( (arc4random() % 49) + 1 )
tabl[5] = Int( (arc4random() % 49) + 1 )

print(tabl)

// ------------------------ //

var tablStr = [String]()
var tablInt = [Int]()
var multi = [[Int]]()

var fruit1 = ["banana", "orange"]
var fruit2 = [String]()

fruit1.append("apple")
//fruit2.append("kivi")
print(fruit1)
print(fruit2)

fruit1.append(contentsOf: fruit2)
print(fruit1)


// ------------------------ //
//dictionaries

var currencies = ["GBP":5.1, "USD":3.5]
var pln = 123.0
print("\(pln) PLN = \((pln/currencies["GBP"]!)) GBP ")
print("\(pln) PLN = \((pln/currencies["USD"]!)) USD ")

if let gbp = currencies["GBP"]{
    print("\(pln) PLN = \((pln/gbp)) GBP ")
}


// ------------------------ //
// loops

for x in table {
    print(x)
}

for x in 1 ..< 11 {
    print(x)
}

for x in 1 ... 10 {
    print(x)
}

var sum = 0
for element in table {
    sum = sum + element
}

var sum2 = 0
for ind in 0 ..< table.count {
    sum2 = sum2 + table[ind]
}

// ------------------------ //
let credit = 200_000.0
let years = 10
let percentage = 0.05

var overall = 0.0
var left = credit

for month in 1 ... years * 12 {
    let capital = credit / Double(years * 12)
    let interest = (left * percentage)/12.0
    
    let capitalAndInteres = capital + interest
    
    print("Month \(month): \(capitalAndInteres) PLN [C: \(capital), I: \(interest)]")
    left -= capitalAndInteres
    overall += capitalAndInteres
}
print("Overall: \(overall)")


// ------------------------ //

var month = 0
let wanted = 900.0
var overall2 = 0.0
while ( left > 0 ) {
    month += 1
    let interest = (left * percentage)/12.0
    let capital = wanted - interest
    left -= capital
    
    print("Month \(month): [C: \(capital), I: \(interest)]")
    
    overall2 += interest + capital
    print(overall2)
    
}


// ------------------------ //
// algorythms

//bubble sort
var tab = [2,6,1,8,3,7,1,6,2,42,3,34,37,17,34,23,25,26,27]
for _ in 1 ... tab.count {
    for ind in 0 ..< ( tab.count - 1 ) {
        let ind1 = ind
        let ind2 = ind + 1
        
        if (tab[ind1]>tab[ind2]){
            (tab[ind1],tab[ind2]) = (tab[ind2],tab[ind1])
        }
    }
}
print(tab)

tab.sort()
tab.suffix(5)
tab.prefix(3)
tab.reverse()
tab.insert(511, at:5)
tab.remove(at: 11)


// ------------------------ //
// functions

func randomGame(max:Int, count: Int) -> [Int] {
    var tabs = [Int]()
    
    func draw() -> Int{
        return Int(arc4random()) % max + 1
    }
    
    while ( tabs.count < count ) {
        var number:Int = 0
        repeat {
            number = draw()
        } while tabs.contains(number)
        tabs.append(number)
    }
    
    return tabs
}

randomGame(max: 30, count: 3)


//function returning function
func createGame(max: Int) -> ((Int)->[Int]){
    func randomGame(count: Int) -> [Int] {
        var tabs = [Int]()
        
        func draw() -> Int{
            return Int(arc4random()) % max + 1
        }
        
        while ( tabs.count < count ) {
            var number:Int = 0
            repeat {
                number = draw()
            } while tabs.contains(number)
            tabs.append(number)
        }
        
        return tabs
    }
    return randomGame
}

let lotto = createGame(max: 49)
lotto(6)

let multi2 = createGame(max: 80)
multi2(8)


func play(howmuch: Int, todraw: Int, game: ((Int)->[Int])) ->[[Int]] {
    var coupon = [[Int]]()
    for _ in 1 ... howmuch {
        var drawed = game(todraw)
        coupon.append(drawed)
    }
    return coupon
}

play(howmuch: 6, todraw: 6, game: lotto)


// domknięcia

play(howmuch: 5, todraw: 4, game: {num in
    var tab = [Int]()
    for _ in 1 ... num {
        tab.append(1)
    }
    return tab

})

play(howmuch: 5, todraw: 4) { num in
    var tab = [Int]()
    for _ in 1 ... num {
        tab.append(1)
    }
    return tab
}

// ------------------------ //

struct Point {
    var x:Int, y:Int
}
Point(x: 3, y: 4)


struct Employee {
    var name:String
    var surname:String
    var earning:Double
    var age:Int
}

Employee(name: "john", surname: "doe", earning: 123.45, age: 30)


struct Movie {
    var name:String
    var director:String
    var year:Int
    
    func writeInfo(){
        print("Director: \(director), name: \(name), year: \(year)")
    }
}

var movie = Movie(name: "abc", director: "dir", year: 2018)

var movies = [Movie]()

movies.append(Movie(name: "abc", director: "dir1", year: 2018))
movies.append(Movie(name: "cde", director: "dir2", year: 2017))
movies.append(Movie(name: "efg", director: "dir3", year: 2012))

func writeInfo(movie:Movie){
    print("Director: \(movie.director), name: \(movie.name), year: \(movie.year)")
}

for m in movies {
    //writeInfo(movie: m)
    m.writeInfo()
}


// ------------------------ //
//classes

class Instrument {
    var sound:String
    var name:String
    init(_sound:String, _name:String){
        self.sound = _sound
        self.name = _name
    }
}

var cello = Instrument(_sound: "high", _name: "cello")

 

class Car{
    var noOfWheels:Int
    var maxSpeed:Int
    init(noOfWheels:Int, maxSpeed:Int){
        self.noOfWheels = noOfWheels
        self.maxSpeed = maxSpeed
    }
    
    func wrr(){
        print("wrrrum")
    }
}

class Maluch:Car{
    var reliable:Bool
    init(noOfWheels:Int, maxSpeed:Int, reliable:Bool){
        self.reliable = reliable
        super.init(noOfWheels: noOfWheels, maxSpeed: maxSpeed)
    }
    
    override func wrr() {
        print("brbrbrbr")
    }
}

class Ferrari:Car{
    var price:Int
    
    init(noOfWheels:Int, maxSpeed:Int, price:Int){
        self.price = price
        super.init(noOfWheels: noOfWheels, maxSpeed: maxSpeed)
    }
}

let c1 = Ferrari(noOfWheels: 4, maxSpeed: 300, price: 1_000_000)
let c2 = Maluch(noOfWheels: 4, maxSpeed: 80, reliable: false)

c1 is Car
c1 is Maluch
c1 is Ferrari

func gasStation(car: Car){
    print("tank")
}

gasStation(car: c1)

c1.wrr()
c2.wrr()


let c3: Car = c2
c3.wrr()



// ------------------------ //
// references
// automatic reference counting

class Human {
    var name:String
    weak var knows:Human? //żeby nie zwiększać licznika referencji
    init(name:String){
        self.name = name
        print("created obj \(name)")
    }
    deinit {
        print("deleting object")
    }
}

var h:Human? = Human(name: "john doe")
//var jj = h //wzrasta licznik referencji
weak var jj = h //nie wzrasta licznik referencji
h = nil

//zmienna weak nie ma wpływu na zwiększenie licznika referencji

var human1:Human? = Human(name: "brad")
var human2:Human? = Human(name: "johny")

//jeśli knows NIE będzie weak var => wyciek pamięci
//nawet po skasowaniu obiektu (human1=nil) poniższe spowoduje że te obiekty dalej będą w pamięci, ale bez dostępu do nich
human1?.knows = human1
human2?.knows = human1

human1 = nil
human2 = nil


// ------------------------ //
// objects

class Citizen {
    func scream(){
        print("aaaaaaa")
    }
}

class Cook:Citizen{
    var sheep = Sheep()
    func feedDragon(dragon: Dragon){
        dragon.eat(sheep: self.sheep)
    }
}

class Dragon {
    var sleeping = false
    func hello(citizen: Citizen){
        if (!sleeping){
            print("fireee agrrrr")
            citizen.scream()
        } else {
            print("zzZZzz")
        }
    }
    
    func eat(sheep:Sheep){
        print("Omnomonomnom")
        sleeping = true
    }
}

class Sheep {
    
}

var cc1 = Citizen()
var cc2 = Citizen()
var cc3 = Citizen()

var d = Dragon()

d.hello(citizen: cc1)
d.hello(citizen: cc2)
d.hello(citizen: cc3)

var c4 = Cook()
d.hello(citizen: c4)

c4.feedDragon(dragon: d)

var c5 = Citizen()
d.hello(citizen: c5)


