//
//  ViewController.swift
//  Picture Gallery
//
//  Created by Blazej Zyglarski on 11.07.2017.
//  Copyright © 2017 Blazej Zyglarski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let t1 = UITapGestureRecognizer(target: self, action: #selector(ViewController.prevPressed))
        prevRef.addGestureRecognizer(t1)
        
        let t2 = UITapGestureRecognizer(target: self, action: #selector(ViewController.nextPressed))
        nextRef.addGestureRecognizer(t2)
        
    }
    
    var actual = 1
    @objc func prevPressed(){
        actual -= 1
        if actual == 0 {
            actual = 5
        }
        self.imgDisplay.image =
        UIImage(named: "p-\(actual)")
    }
    @objc func nextPressed(){
        actual += 1
        if actual == 6 {
            actual = 1
        }
        self.imgDisplay.image =
            UIImage(named: "p-\(actual)")
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var prevRef: UIImageView!
    @IBOutlet weak var nextRef: UIImageView!
    
    @IBOutlet weak var imgDisplay: UIImageView!
}

