//
//  ViewController.swift
//  Where am I
//
//  Created by Blazej Zyglarski on 12.07.2017.
//  Copyright © 2017 Blazej Zyglarski. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    var cl :CLLocationManager?
    
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cl = CLLocationManager()
        cl?.requestAlwaysAuthorization()
        
        mapView.userTrackingMode = .follow
        
        //cl?.location
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

