//
//  ViewController.swift
//  Picture Gallery
//
//  Created by Blazej Zyglarski on 11.07.2017.
//  Copyright © 2017 Blazej Zyglarski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var data : Candidates?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlstring = "http://zyglarski.pl/swift-http-docs/tinder_list.php"
        
        let content = try! String(contentsOf: URL(string: urlstring)!)
        
        let contentData = content.data(using: .utf8)
        let jsDec = JSONDecoder()
        data = try! jsDec.decode(Candidates.self, from: contentData!)
        
        
        let t1 = UITapGestureRecognizer(target: self, action: #selector(ViewController.prevPressed))
        prevRef.addGestureRecognizer(t1)
        
        let t2 = UITapGestureRecognizer(target: self, action: #selector(ViewController.nextPressed))
        nextRef.addGestureRecognizer(t2)
        
    }
    
    var actual = 1
    @objc func prevPressed(){
        actual += 1
        if actual == data?.candidates.count {
            actual = 1
        }
        displayInfo(index: actual)
    }
    @objc func nextPressed(){
        actual += 1
        if actual == data?.candidates.count {
            actual = 1
        }
        displayInfo(index: actual)
    }

    func displayInfo(index:Int){
        //
        let displaydata = data?.candidates[index-1]
        let filename = displaydata?.filename
        
        let url = "http://zyglarski.pl/swift-http-docs/pictures/\(filename!)"
        
        let img = try! UIImage(data:
            Data(contentsOf:
                URL(string: url)!
            )
        )
        
        imgDisplay.image = img
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var prevRef: UIImageView!
    @IBOutlet weak var nextRef: UIImageView!
    
    @IBOutlet weak var imgDisplay: UIImageView!
}

struct Candidates: Codable{
    let candidates:[Candidate]
}
struct Candidate: Codable{
    let filename:String
    let name:String
    let age:String
    let distance:String
    let match:String
}

