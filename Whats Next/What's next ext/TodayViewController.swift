//
//  TodayViewController.swift
//  What's next ext
//
//  Created by Blazej Zyglarski on 16.06.2018.
//  Copyright © 2018 Blazej Zyglarski. All rights reserved.
//

import UIKit
import NotificationCenter
import EventKit


class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var eventTitle: UILabel!
    
    @IBOutlet weak var eventDate: UILabel!
    
    @IBOutlet weak var eventAlarmSwitch: UISwitch!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view from its nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    var displayedEvent:EKEvent?
    var eventStore: EKEventStore?
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        
        let eks = EKEventStore()
        eventStore = eks            /**/
        
        eks.requestAccess(to: .event) {approved,_ in
            if(approved){
                let cals = eks.calendars(for: .event)
                let pred = eks.predicateForEvents(withStart: Date(), end: Date().addingTimeInterval(60.0*60.0*24.0), calendars: cals)
                
                let events = eks.events(matching: pred)
                
                if let event =  events.first {
                    self.displayedEvent = event
                    self.eventTitle.text = event.title
                    self.eventDate.text = event.startDate.description
                    
                    if let _ = event.alarms?.first{
                        self.eventAlarmSwitch.setOn(true, animated: true)
                    }else{
                        self.eventAlarmSwitch.setOn(false, animated: true)
                    }
                    
 
                }else{
                    self.eventTitle.text = "Brak"
                    self.eventDate.text = "---"
                }
                
            }
        }
        
        
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        
        
        completionHandler(NCUpdateResult.newData)
    }
    
    
    @IBAction func switchTriggered(_ sender: Any) {
        
        if let uiswitch  = self.eventAlarmSwitch{
            if uiswitch.isOn{
                print("ustaw alarm")
                var alarm = EKAlarm(relativeOffset: -60.0*60.0)
                self.displayedEvent?.addAlarm(alarm)
            }else{
                print("skasuj alarm")
                self.displayedEvent?.alarms = nil
            }
            
            do{
            try self.eventStore?.save(self.displayedEvent!, span: .thisEvent)
            }catch{
                
            }
            
        }
        
        
        
    }
    
    
    
    
    
    
}
