//
//  DetailViewController.swift
//  diaryApp
//
//  Created by dagmara on 16/06/2018.
//  Copyright © 2018 d. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var detailContentTextView: UITextView!
    
    func textViewDidChange(_ textView: UITextView) {
        if let detail = detailItem {
            detail.content = textView.text
            do {
                try detail.managedObjectContext?.save()
            } catch {}
        }
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.timestamp!.description
            }
            if let tv = detailContentTextView {
                tv.text = detail.content
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Event? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

