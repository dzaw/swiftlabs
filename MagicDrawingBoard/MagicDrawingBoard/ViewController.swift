//
//  ViewController.swift
//  MagicDrawingBoard
//
//  Created by Blazej Zyglarski on 23.01.2017.
//  Copyright © 2017 Blazej Zyglarski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var obrazekdisplay: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let g1 = UITapGestureRecognizer(target: self, action: #selector(ViewController.buttonNewClicked))
        buttonNew.addGestureRecognizer(g1)
        let g2 = UITapGestureRecognizer(target: self, action: #selector(ViewController.buttonPhotoClicked))
        buttonPhoto.addGestureRecognizer(g2)
        let g3 = UITapGestureRecognizer(target: self, action: #selector(ViewController.buttonLoadClicked))
        buttonLoad.addGestureRecognizer(g3)
        
        //let g4 = ZADANIE 1 napisz wywołanie dla buttonSaveClicked
        let g4 = UITapGestureRecognizer(target: self, action: #selector(ViewController.buttonSaveClicked))
        buttonSave.addGestureRecognizer(g4)
        
        createNewImage()
    }
    func buttonPhotoClicked(){
        
        //ZADANIE 2 Napisz zapisywanie obrazka do zdjęć
        let myImage = self.obrazekdisplay.image
        
        UIImageWriteToSavedPhotosAlbum(myImage!, nil, nil, nil)
        
    }
    func buttonSaveClicked(){
        // ZADANIE 3 Napisz zapisywanie obrazka do pliku
        print("save clicked")
        let url = createPath()
        let myImage = self.obrazekdisplay.image
        
        //np. UIImageJPEGRepresentation(.. albo UIImagePNG..
        let data = UIImagePNGRepresentation(myImage!)
        do {
            try data?.write(to: url!)
        } catch {
            
        }
    }
    
    
    func buttonNewClicked(){
        createNewImage()
    }
    
    func buttonLoadClicked(){
        
        createNewImage() // tworzy nowy pusty kontekst
        let url = createPath()
        
        let image = try! UIImage(data: Data(contentsOf: url!))
        image?.draw(at: CGPoint(x:0, y:0)) // = image?.draw(at: .zero)
        // ZADANIE 4 Napisz pobieranie obrazka z pliku
        
        updateImage()
        
    }
    
    func createPath()->URL?{
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        
        if let url = urls.first {
            print(url)
            return url.appendingPathComponent("saved.png")
        }
        
        return nil
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var color = UIColor.black
    @IBAction func colorTouched(_ sender: UIButton) {
        //print(sender)
        
        
        
        
        switch sender.tag{
        case 1: color = UIColor.red
        case 2: color = UIColor.yellow
        case 3: color = UIColor.black
        case 4: color = UIColor.green
        case 5: color = UIColor.blue
        default: color = UIColor.black
        }
        
        print(color)
        
        
    }
    
    @IBOutlet weak var buttonNew: UIImageView!
    
    @IBOutlet weak var buttonLoad: UIImageView!
    @IBOutlet weak var buttonPhoto: UIImageView!
    
    @IBOutlet weak var buttonSave: UIImageView!
    
    
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let p0 = touches.first?.previousLocation(in: self.obrazekdisplay)
        let p1 = touches.first?.location(in: self.obrazekdisplay)
        
        context?.setLineWidth(10.0)
        context?.setStrokeColor(color.cgColor)
        context?.move(to: p0!)
        context?.addLine(to: p1!)
        context?.strokePath()
        
        
        
        
        updateImage()
    }
    
    
    
    
    var context: CGContext?
    func createNewImage(){
        UIGraphicsBeginImageContext(self.obrazekdisplay.frame.size)
        context = UIGraphicsGetCurrentContext()
        updateImage()
    }
    
    func updateImage(){
        let uiimage = UIImage(cgImage: (context?.makeImage())!)
        
        self.obrazekdisplay.image = uiimage
        
    }
    
    

}

